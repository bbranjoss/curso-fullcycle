const express = require('express');
const mysql = require('mysql');
const app = express();
const port = 8080;
const config = {
    host: 'db',
    user: 'root',
    password: 'passr',
    database: 'nodedb'
};
let nomes;

function connect() {
    
    const connection = mysql.createConnection(config);
    return connection
}

function createPerson() {

    const connection = connect();

    let sql = `INSERT INTO people(name) VALUES ('Bruno')`;
    return connection.query(sql);
}

function getPersonList() {
    
    const connection = connect();

    sql = `SELECT * FROM people ORDER BY id`;
    connection.query(sql, function(err, result, fields) {
        if (err) 
            throw err
        nomes = result
    });
    return nomes
}

app.get('/', (req,res) => {
  
    createPerson();
    getPersonList();
    let content = '';

    try {
        content += '<h1>Full Cycle Rocks!</h1>';
        content += '<h2>Total de nomes cadastrados: ' + nomes.length + '</h2>';

        for(i=0; i<nomes.length; i++) {
            content += nomes[i]['id'] + ': ' + nomes[i]['name'] + '<br>';
        }

        res.send(content)
    } catch (e) {
        res.send('<h1>Por favor recarregue a página</h1>')
    }
});

app.listen(port, () => {
    console.log('Rodando na porta ' + port);
});
